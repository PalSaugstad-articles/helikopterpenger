.. raw:: html

   <!-- (php # 2 __DATE__ Helikopterpenger) -->
   <!-- ($attrib_AC=0;) -->

================
Helikopterpenger
================

*Av: |author| / |datetime|*

.. include:: revision-header.rst

..
   2016-10-01-kla-43 Anton Hellesøy, I påvente av krisa
   2016-09-28-kla-20 Lars Hektoen, Pengenes kretsløp
   2016-09-27-kla-22 Anton Hellesøy, Penger ut av løse luften
   2016-09-23-kla-18 Lars Hektoen, Pengegaloppen
   2016-09-16-kla-28 Lars U. Larsen Vegstein, Hvordan skaper vi penger?

Klassekampen har ved flere anledninger trykket kronikker og leserinnlegg om penger og hvordan de blir skapt.
Jeg har interessert meg for dette i flere år, og nå vil jeg gjerne presentere en versjon som er basert på
det personer som Adair Turner, Mary Mellor, Bernard Litetaer og Silvio Gesell har skrevet og sagt.

Statistisk sentralbyrå holder statistikker over pengemengden og kredittmengden i Norge.
Pengemengden representerer det som folk og bedrifter samlet har av penger på konto,
mens kredittmengden det som folk og bedrifter samlet har i lån.
Andre land har tilsvarende statistikker.
Pengemengden er delt inn i klasser, M0 - M3, hvor høyere nummer betyr at flere klasser av penger er inkludert.
Kredittindikatoren er delt inn på tilsvarende måte (K2 - K3).

Grovt sett har vi K = M, altså summen av alle lån (Kredittindikatoren) er lik summen av alle pengene (Pengemengden) på konto.
Dette skyldes at penger blir skapt som en følge av at penger lånes.
I virkeligheten er bildet enda verre, K er faktisk større enn M.
Dette kan spores i bankenes balanse når det gjelder utlån sammenliknet med innlån.
En større andel av forvaltningskapitalen på aktiva-siden er utlån til kunder enn innskudd fra kunder på passiva-siden.
Jeg vil likevel argumentere ut fra at vi setter K = M.
Blir K større (noen låner penger),
så blir M større (det blir mer penger på konto), og når K minker (noen betaler avdrag),
så blir M mindre (det er kontopenger som brukes for å betale avdrag).
(Se Klassekampen 2016-09-27 side 22 *'Penger ut av løse luften' av Anton Hellesøy*.)

Hvis bankene i mindre grad yter kreditt pga. antatt fare for mislighold, så minker genereringen av nye penger
som nåværende låntakere er avhengig av å få tak i for å betale tilbake sine lån.

Hvis for eksempel boligboblen sprekker, så vil folk ikke oppta store lån lenger, og K øker mindre enn før, evt. krymper.
Da vil M ikke øke evt. krympe også, hvilket er starten på en deflasjonsspiral:
Folk får etterhvert lavere lønninger som skal gå til avdrag på enda
mer tyngende lån som vil føre til enda høyere andel av misligholdte lån.
Bankene vil fort bruke opp reservene sine og må enda engang sannsynligvis reddes vha. ekstraordinære redningspakker.
Kravet til økte reserver i bankene for å stå imot dårlige tider virker faktisk også tildels mot sin hensikt.
På bankbalansens passivaside kommer egenkapitalen inn og tenderer til å komprimere andelen som er innskudd fra kunder ytterligere,
slik at misforholdet mellom K og M blir enda større.
Resultatet av dette er at bankene i og for seg står sterkere *hvis* en krise oppstår,
men sannsynligheten for at en ny krise *oppstår* øker fordi den samlede gjeldsmengden øker i forhold til den samlede pengemengden.

Disse betraktningene peker også på grunnen til at økonomien ikke tåler negativ vekst.
Det er en kjent sak i bankkretser at det finansielle systemet
kan virker selvforsterkende; dårlige tider kan bli dårligere enn nødvendig
(se Norges Bank Skriftserie nr 34, 2004 side 27).
Sentralbankenes virkemidler når veksten avtar er først å sette ned renten,
så å starte med kvantitativ lettelse
(se Klassekampen 2016-08-18 side 21 *'Hvor super er Mario?' av Philip Róin*).

Hva for noe nytt bringer så helikopterpenger inn?
Jo, helikopterpenger slik som begrepet blir brukt i fagkretser,
betyr penger spredd utover i økonomien uten at det finnes noen motsvarende like stor gjeldspost.

Ved innføring av helikopterpenger kan vi bytte ut formelen K = M med K + H = M, der H står for helikopterkreditt.
Begrepet helikopterkreditt er ikke vanlig å bruke av talspersonene for helikopterpenger.
Jeg synes begrepet gir mening fordi hvis vi innfører helikopterpenger,
så trenger vi å kunne tallfeste hvor mye av det vi har innført.
Vanlig tankegang innen bankkretser opp til nå er at det umulig kan finnes penger uten at det også finnes en
motsvarende post med motsatt fortegn. All moderne regnskapsførsel er basert på dette prinsippet.
H, helikopterkreditten kan ses på som evigvarende rentefri kreditt som *samfunnet yter til sine innbyggere*.
Og ja, vi har allerede tatt i bruk rentefri evigvarende kreditt:
Når sentralbanken utsteder sedler og mynt, ses disse på som rentefrie evigvarende gjeldsbrev.
De kontopengene som innbyggerene betaler for sedlene, kan bankene tjene penger på
uten at noe av denne fortjenesten blir tilgodesett den som holder på seddelen.
Seddelen du holder i hånda representerer altså det sentralbanken skylder deg i kontopenger
(før i tiden ikke kontopenger, men den vedtatte mengden gull eller sølv som pengeseddelen representerte).

Flere talspersoner for helikopterpenger mener at de bør være administrert av sentralbanken
og betalt ut til staten slik at de kan inngå i statsbudsjettet.
Hvordan denne operasjonen skal "forklares" er det delte meninger om.
Flere mener at dette tilsvarer at staten setter opp et underskuddsbudsjett
som blir dekket opp med nyskapte gjeldfrie penger.
Mange diskuterer om det er noen prinsippiel forskjell mellom helikopterpenger
og kvantitativ lettelse (QE).
Adair Turner har sagt at det er liten forskjell, bortsett fra at QE i
utgangspunktet er ment å skulle bli reversert,
at statsobligasjoner som sentralbanken kjøpte som en motpost til QE blir inndradd igjen.
Men hvis, sier han, at man senere bestemmer seg for aldri å dra inn igjen disse statsobligasjonene,
så er det ingen forskjell.
Mary Mellor mener at det *er* en forskjell, men at begge deler er gjeldfrie penger, siden
QE ikke manifesterer seg som en gjeldspost "eid av" publikum. Forskjellen, sier hun,
er at QE er sentralbankens innpumping av penger i banksystemet som bankene så kan låne ut,
gitt at de finner personer som ønsker å låne dem.
Ofte, sier hun, så er resultatet at banken lar
disse pengene stå på konto i sentralbanken i stedet for å låne dem ut.
Videre sier hun at helikopterpenger derimot, går rett ut i realøkonomien og at dette derfor
gir en mye bedre effekt på økonomien.

I min forståelse av penger kan dette uttrykkes på en noe annen måte, uten at jeg dermed
mener at det Mary Mellor og Adair Turner sier nødvendigvis er feil.
Saken er nemlig den at mange ting her i verden kan ses på fra forskjellige vinkler,
og at det å innføre en annen betraktning i mange tilfeller gjør at de underliggende
sammenhengene trer klarere fram.

Penger og kreditt er *informasjon om* hvilken tilstand hver enkelt aktør er i i realøkonomien.
Dersom du har penger på konto, så har du muligheten til å skaffe deg saker og ting.
Ved hjelp av låneopptak kan man forskyve disse mulighetene
fra noen til noen andre.
Formelen K + H = M viser at både opptak av lån i bank og helikopterkreditt vanner ut pengemengden.
Så hvis du går i banken og låner 5 millioner kroner til kjøp av hus,
så bidrar du samtidig litt til en etterfølgende prisstigning
fordi du vanner ut pengemengden med 5 millioner.
**Vi overfører mao. kjøpekraft fra de eksisterende pengene til de nyskapte pengene
både ved låneopptak via bank og ved å skape nye penger vha. helikopterkreditt**.

Vi kan altså begrunne K med at visse individer til visse tider trenger mer enn normalt med penger
for å skaffe seg et varig objekt som f. eks. et hus.
H begrunnes med at så lenge vi er mennesker som ønsker utstrakt bruk av arbeidsdeling
så trenger vi penger for å holde rede på hvor langt vi har kommet i byttehandelene med hverandre.
Selv i et samfunn der store investeringer ikke trengs,
f. eks. fordi boligforholdene er organisert kun via utleie,
vil det være behov for penger pga. arbeidsdelingen.
Med denne betrakningen kan vi se på et statsbudsjett i underskudd som blir
støttet opp med helikopterpenger på en ny måte:
Det som skjer er at publikums kjøpekraft blir inndradd ved å vanne ut pengemengden og at
dette deretter kommer publikum tilgode igjen via de ytelsene fornuftig bruk at offentlige midler kan gi.
Samtidig innser vi at skattene kan settes noe lavere
fordi denne utvanningen gir et bidrag i tillegg til generelle skatter.

Ved innføring av helikopterpenger vil altså
pengemengden (M) bestå av K (kreditt som bankene yter til publikum) pluss H (den evigvarende rentefrie kreditten).
Da er det ikke lenger fullt så kritisk at K blir mindre, for med H kan man likevel sikre at M ikke skrumper helt inn,
og på den måten eliminere en vesentig årsak til deflasjon og høy gjeldsbyrde.

Et alternativ til å tilgodese befolkningen indirekte via statsbudsjettet
er å dele ut helikopterpengene flatt direkte til alle innbyggere
(se Klassekampen 2016-07-12 side 2 *'Økonomisk sannhet' av Ebba Boye*).
Jeg synes hovedpoenget er at helikopterpengene vil forbedre forholdet mellom kredittmengden og pengemengden.
På hvilken måte de starter sin sirkulasjon er også interessant
men dette hører til i en diskusjon der man ser på hvordan vi kan jevne ut forskjellene i samfunnet.
Direkte pengestøtte er bare ett av mange virkemidler her.

Vårt nåværende pengesystem fungerer helt utmerket så lenge vi har høy økonomisk vekst
og høyt investeringsbehov.
Da ligger styringsrenten til sentralbanken i et område der den faktisk har reell påvirkning på økonomien,
det er ingen behov for kvantitativ lettelse eller helikopterpenger,
inflasjonen er passe høy og hjelper til med å spise opp gamle lån.
En andel av pengene som skapes i banksystemet kan uten store problemer trekkes inn vha. skatter og
statsbudsjettene kan derfor settes opp i balanse, hverken overskudd eller underskudd.
Nye penger blir hele tiden generert i banksystemet og hele økonomien fungerer fint.
Hvis vi derimot har lav vekst eller negativ vekst fører dette til
at vi får en gjeldsmengde som etterhvert blir helt umulig å betjene
og som derfor kommer til å ende i en gedigen bank-kollaps.

Når økonomer diskuterer helikopterpenger, Adair Turner inkludert,
er det nesten utelukkende i retning av at
de håper det vil stimulere etterspørselen og dermed føre til økt vekst.
Jeg tror det er andre mekanismer som har sterkere virkning på etterspørselen enn
detaljer i hvordan pengesystemet er bygd opp.
I et scenario hvor det går veldig tregt å få igang ny vekst,
vil helikopterpenger kunne bidra positivt i seg selv
fordi kredittmengden kan holdes lav i forhold til
pengemengden.
For Norges del kan den potensielle gjeldsboblen som skyldes den potensielle boligboblen
bli krevende å få kontroll på.
Jeg er i alle fall veldig bekymret.
Helikopterpenger kan være et alternativ eller tillegg til satsingen på fortsatt økonomisk vekst.


.. raw:: html

   <hr>
   <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=Helikopterpenger">Tweet</a>
   <br>
   <a target="_blank" href="show?f=articles/parsed/helikopterpenger/nor/helikopterpenger.pdf">som pdf</a>
   <hr>

Kilder
^^^^^^

I det siste har pengesystemer blitt belyst i Klassekampen:

* 2016-09-16 side 28: Lars U. Larsen Vegstein/Anton Hellesøy, Hvordan skaper vi penger?
* 2016-09-23 side 18: Lars Hektoen, Pengegaloppen
* 2016-09-27 side 22: Anton Hellesøy, Penger ut av løse luften
* 2016-09-28 side 20: Lars Hektoen, Pengenes kretsløp
* 2016-10-01 side 43: Anton Hellesøy, I påvente av krisa

Youtube
^^^^^^^

* `Mary Mellor (1t 21min) <https://www.youtube.com/watch?v=9F-DuD6T_i0>`_
* `Adair Turner (paneldebatt) (2t 3min) <https://www.youtube.com/watch?v=UUsd-7gF0kc>`_
